//
//  ContentView.swift
//  serverxc
//
//  Created by milad mostafa on 11/24/1398 AP.
//  Copyright © 1398 AP milad mostafa. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
